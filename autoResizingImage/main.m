//
//  main.m
//  autoResizingImage
//
//  Created by osx on 01/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
